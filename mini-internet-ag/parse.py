import os
import json

def main():
    tests = []
    if os.path.isfile("submission/report.pdf"):
        tests.append({
            "name": "Report submission",
            "status": "passed",
        })
    else:
        tests.append({
            "name": "Report submission",
            "status": "failed",
            "output": "report.pdf file not found. Please submit with the correct file name and file type!",
        })
        
    
    required_files = [
        'BASE.txt', 'GENE.txt', 'LUGA.txt', 'LYON.txt', 'MILA.txt',
        'MUNI.txt', 'S1.txt', 'S2.txt', 'S3.txt', 'S4.txt',
        'VIEN.txt', 'ZURI.txt'
    ]
    all_files_found, found_files = find_files('submission', required_files)
    if all_files_found:
        tests.append({
            "name": "Router configs submission",
            "status": "passed",
        })
    else:
        missing_files = [file for file, found in found_files.items() if not found]
        tests.append({
            "name": "Router configs submission",
            "status": "failed",
            "output": f"The following files are missing: {', '.join(missing_files)}",
        })

    os.makedirs('results', exist_ok=True)
    with open('results/results.json', 'w') as file:
        json.dump({"score": 0, "tests": tests}, file, indent=4)


# Function to search for files
def find_files(base_directory, required_files):
    found_files = {file: False for file in required_files}
    
    for root, dirs, files in os.walk(base_directory):
        for file in files:
            if file in found_files:
                found_files[file] = True
                
    return all(found_files.values()), found_files

if __name__ == "__main__":
    main()
